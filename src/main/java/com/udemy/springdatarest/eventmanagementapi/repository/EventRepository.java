package com.udemy.springdatarest.eventmanagementapi.repository;

import com.udemy.springdatarest.eventmanagementapi.entity.Event;
import com.udemy.springdatarest.eventmanagementapi.entity.projections.PartialEventProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.security.access.prepost.PreAuthorize;

import java.time.ZoneId;
import java.util.List;

// using this annotation, if we go to URL http://localhost:8081/eventmanagement-api/events,
// we will receive the collection of partial events
// (only fields, mention in PartialEventProjection.class, will be shown)
@RepositoryRestResource(excerptProjection = PartialEventProjection.class)
public interface EventRepository extends PagingAndSortingRepository<Event, Long> {

    // through URL we search like:
    // http://localhost:8081/eventmanagement-api/events/search/findByName?name=South Park city tour 4
    // where "South Park city tour 4" - the searched value in column "name"

    // List<Event> findByName(@Param("name") String name); // non-pageable variant
    Page<Event> findByName(@Param("name") String name, Pageable pageable); // pageable variant

    Page<Event> findByNameAndZoneId(@Param("name") String name, @Param("zoneId") ZoneId zoneId, Pageable pageable);

    // @PreAuthorize annotation is used to configure method level security for delete() method
    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    void delete(Long id);
}
