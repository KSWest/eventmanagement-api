package com.udemy.springdatarest.eventmanagementapi.repository;

import com.udemy.springdatarest.eventmanagementapi.entity.Organizer;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface OrganizerRepository extends PagingAndSortingRepository<Organizer, Long> {
    
}
