package com.udemy.springdatarest.eventmanagementapi.repository;

import com.udemy.springdatarest.eventmanagementapi.entity.Venue;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface VenueRepository extends PagingAndSortingRepository<Venue, Long> {
    
}
