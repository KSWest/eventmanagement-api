package com.udemy.springdatarest.eventmanagementapi.repository;

import com.udemy.springdatarest.eventmanagementapi.entity.Participant;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface ParticipantRepository extends PagingAndSortingRepository<Participant, Long> {
    
}
