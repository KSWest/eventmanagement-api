package com.udemy.springdatarest.eventmanagementapi.entity.projections;

import com.udemy.springdatarest.eventmanagementapi.entity.Event;
import org.springframework.data.rest.core.config.Projection;

import java.time.Instant;
import java.time.ZonedDateTime;

// URL to get projection of events:
// http://localhost:8081/eventmanagement-api/events?projection=partial
// return each event in a format:

//"name": "South Park INC",
//"endTime": "2017-08-29T19:46:18+03:00",
//"startTime": "2017-08-29T17:46:18+03:00"
//"created": "2017-12-11T15:04:04Z"     // even @JsonIgnore-marked fields could be included in projection

@Projection(name = "partial",types = {Event.class})
public interface PartialEventProjection {

    String getName();

    ZonedDateTime getStartTime();

    ZonedDateTime getEndTime();

    Instant getCreated();
}
